## Interactive 16S data analysis

This is a web app built with the R packages `shiny` and `ggvis` designed for interactive analysis of 16S microbiome data.  To use the app you'll need three files.


1. The OTU table in biom format.  This is the only format supported for now.
2. A map file, preferably one with your metadata.  Make sure to get rid of that pesky '#' that Qiime likes to put on the header line.
3. A tree file, should be midpoint rooted.

The easiest way to run the app is to make sure both `server.R` and `ui.R` are in the same directory and open either of the files in the latest version of RStudio.  Then click the 'Run App` button in the top right corner of the source window.

You can also host the app using Shiny Server.  See http://www.rstudio.com/products/shiny/shiny-server/ for more details.